﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector v1;
            Vector v2;
            int[] input = new int[2];

            Console.Write("Vector 1. - X: ");
            input[0] = Convert.ToInt32(Console.ReadLine());
            Console.Write("          - Y: ");
            input[1] = Convert.ToInt32(Console.ReadLine());
            v1 = new Vector(input[0], input[1]);

            Console.Write("\nVector 2. - X: ");
            input[0] = Convert.ToInt32(Console.ReadLine());
            Console.Write("          - Y: ");
            input[1] = Convert.ToInt32(Console.ReadLine());
            v2 = new Vector(input[0], input[1]);

            Console.Write("==============\n");
            Console.Write(v1.Add(v2).ToString());

            Console.ReadKey();
        }
    }
}
