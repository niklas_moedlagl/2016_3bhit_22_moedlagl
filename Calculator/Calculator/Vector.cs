﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Vector
    {
        private int x, y;

        public int X
        {
            set { x = value; }
            get { return x; }
        }
        public int Y
        {
            set { y = value; }
            get { return y; }
        }

        public Vector(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return this.x + " -=- " + this.y;
        }

        public Vector Add(Vector v)
        {
            return new Vector(this.x + v.x, this.y + v.y);
        }
    }
}
