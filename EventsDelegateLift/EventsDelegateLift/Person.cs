﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsDelegateLift
{
    class Person
    {
        public string Name { get; set; }
        public int Weight { get; set; }

        public Person(string Name, int Weight)
        {
            this.Name = Name;
            this.Weight = Weight;
        }

    }
}
