﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsDelegateLift
{
    class Program
    {
        static void Main(string[] args)
        {
            Lift l = new Lift(500, "76/34-B(A76)");
            l.Ueberladen += LiftUeberladen;

            l.personAdd(new Person("Huawa", 180));
            l.personAdd(new Person("Hugo",280));
            l.personAdd(new Person("Oachkatzl", 62));
            l.personAdd(new Person("Alexander Fasching", 32));
            l.personAdd(new Person("David von Backen", 108));


            Console.ReadKey();
        }

        static void LiftUeberladen(Person p)
        {
            Console.WriteLine("Die schwere Person mit dem Namen: " + p.Name + " muss aussteigen");
        }
    }
}
