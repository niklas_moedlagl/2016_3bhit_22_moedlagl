﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsDelegateLift
{
    class Lift
    {
        public int Maxweight { get; set; }
        public string Id { get; set; }
        int weight = 0;

        public delegate void DelUeberl(Person p);
        public event DelUeberl Ueberladen;
        public Lift(int Maxweight, string Id)
        {
            this.Maxweight = Maxweight;
            this.Id = Id;
        }

        List<Person> list = new List<Person>();
        
        public void personAdd(Person p)
        {
            int sum = 0;
            
            list.Add(p);

            foreach (Person item in list)
            {
                sum += item.Weight;
            }

                
            if (sum > Maxweight)
            {
                list.Sort(delegate (Person a, Person b) { return a.Weight.CompareTo(b.Weight);});
                for (int i = 0; i < list.Count; i++)
                {
                    if (sum - Maxweight <= list[i].Weight)
                    {
                        Ueberladen(list[i]);
                        list.RemoveAt(i);
                        break;
                    }
                }
            }

        }


        //public event Ueberladen()
        //{
        //    Console.Beep();
        //    Console.WriteLine("The actual weight is more than the allowed maximum!");
        //    
        //}
    }
}
