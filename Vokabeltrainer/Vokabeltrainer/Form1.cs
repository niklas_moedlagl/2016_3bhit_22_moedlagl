﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Vokabeltrainer
{
    public partial class Form1 : Form
    {
        private List<Vokabel> list;
        private Panel Entries;

        public Form1()
        {
            InitializeComponent();
            
            //list = ReadFromFile("vokabel.txt");
            //SortEntries(list);
            //CreateEntries(list);
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            CheckEntries();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            if (list != null)
            {
                DeletePanel();
                SortEntries(list);
                CreateEntries(list);
            }
        }

        private List<Vokabel> ReadFromFile(string strFileOrPath)
        {
            List<Vokabel> list = new List<Vokabel>();
            StreamReader sr = new StreamReader(strFileOrPath);


            string Row;
            int Index;

            while (sr.Peek() != -1)
            {
                Row = sr.ReadLine();
                Index = Row.IndexOf(';');
                list.Add(new Vokabel(Row.Substring(0, Index), Row.Substring(Index + 1)));
            }

            sr.Close();
            return list;
        }

        private void CreateEntries(List<Vokabel> listEntries)
        {
            Entries = new Panel();
            Entries.Width = 450;
            Entries.Height = 10;
            Entries.Top = 20;
            
            this.Height = 100;
            this.Controls.Add(Entries);

            int Topspace = 20;

            foreach (Vokabel voc in listEntries)
            {
                Label lblWord = new Label();
                
                lblWord.Text = voc.German;
                lblWord.Width = 120;
                lblWord.Height = 40;
                lblWord.Left = 20;
                lblWord.Top = Topspace;
                Topspace += 10 + lblWord.Height;   

                TextBox txtWord = new TextBox();
                txtWord.Tag = voc;
                
                
                txtWord.Left = lblWord.Left + lblWord.Width + 20;
                txtWord.Top = lblWord.Top;
                txtWord.Width = lblWord.Width;
                txtWord.Height = lblWord.Height;

                this.Height += 10 + lblWord.Height;
                Entries.Height += 10 + lblWord.Height;

                Entries.Controls.Add(lblWord);
                Entries.Controls.Add(txtWord);

                btnCheck.Top = Topspace;
                btnReload.Top = btnCheck.Top;
            }
        }

        private void CheckEntries()
        {
            foreach (Control c in this.Entries.Controls)
            {
                if (c is TextBox)
                {
                    Vokabel v = (Vokabel)c.Tag;

                    if (v.English.ToLower() == c.Text.ToLower())
                    { c.BackColor = Color.LightGreen; }
                    else
                        c.BackColor = Color.Tomato;
                }
            }
        }
        private void DeletePanel()
        {
            this.Controls.Remove(Entries);
        }
        private void SortEntries(List<Vokabel> listEntries)
        {
            Random rnd = new Random();

            foreach (Vokabel v in listEntries)
            {
                v.Rand = rnd.Next(1, listEntries.Count);
            }

            listEntries.Sort(delegate (Vokabel a, Vokabel b)
            {
                return a.Rand.CompareTo(b.Rand);
            });
        }

        private void msiopen_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter="Vokabel-Dateien |*.voc";
            open.Title="DU Faggot wählst jetzt eine Datei";

            if(open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (list !=  null)
                {
                    DeletePanel();
                }
                list = ReadFromFile(open.FileName);
                SortEntries(list);
                CreateEntries(list);
            }

        }

        private void msiclose_Click(object sender, EventArgs e)
        {

            if (list == null)
                this.Close();
                        
                if (MessageBox.Show("Wollen Sie das Programm wirklich beenden?", "Vokabeltrainer beenden?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    this.Close();
           
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (list == null)
                e.Cancel = false;

            else if (MessageBox.Show("Wollen Sie das Programm wirklich beenden?", "Vokabeltrainer beenden?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                e.Cancel = false;
            else
                e.Cancel = true;
        }

        private void msiproperties_Click(object sender, EventArgs e)
        {
            Einstellungen es = new Einstellungen();
            DialogResult result = es.ShowDialog();

            if (result == DialogResult.OK)
            {
                MessageBox.Show("Ok");
            }
            else
                MessageBox.Show("Cancel");

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}