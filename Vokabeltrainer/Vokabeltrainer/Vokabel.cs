﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vokabeltrainer
{
    class Vokabel
    {
        private string Germanword;

        public string German
        {
            get { return Germanword; }
            set { Germanword = value; }
        }
        private string Englishword;

        public string English
        {
            get { return Englishword; }
            set { Englishword = value; }
        }
        private double dRand;

        public double Rand
        {
            get { return dRand; }
            set { dRand = value; }
        }


        public Vokabel(string Germanword, string Englishword)
        {
            German = Germanword;
            English = Englishword;
            Rand = 0;
        }
    }
}