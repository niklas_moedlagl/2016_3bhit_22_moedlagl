﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schule
{
    class Program
    {
        static void Main(string[] args)
        {
            //the following lines are test code
            //---------------------------------
            School schule = new School();
            Class klasse = new Class("3BHIT");
            Student s1 = new Student("Ferdinand", schule, klasse);
            Student s2 = new Student("Franz", schule, klasse);
            Student s3 = new Student("Maria", schule, klasse);
            klasse = new Class("2BHIT");
            s1 = new Student("Fritz", schule, klasse);
            s2 = new Student("Martha", schule, klasse);
            schule.StartFireAlert("Probealarm");
            //---------------------------------
            //end of test code

            Console.ReadKey();
        }
    }
}
