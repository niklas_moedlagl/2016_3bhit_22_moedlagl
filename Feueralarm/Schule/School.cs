﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schule
{
    class School
    {
        public School()
        { }

        public int GetPlaceNumber(Class k)
        {            
            switch (k.Name)
            {                
                case "3BHIT":
                    return 1;
                    

                case "2BHIT":
                    return 2;

                default:
                    return 5;
            }
        }

        public delegate void FireAlert(string typ);
        public event FireAlert Alert;

        public void StartFireAlert(string type)
        {
            Alert(type);
        }
               
    }
}
