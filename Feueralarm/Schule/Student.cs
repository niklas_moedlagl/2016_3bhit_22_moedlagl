﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schule
{
    class Student
    {
        private string name;
        //public string Name { get; set; }
        private Class school_class;
        private School school;

        public Student(string n, School s, Class c)
        {
            this.name = n;
            this.school = s;
            this.school_class = c;
            s.Alert += s_fire;
           
        }
        private void s_fire(string type)
        {
            Console.WriteLine("{0}! Schüler: {1} begibt sich in den Hof {2}", type, this.name, this.school.GetPlaceNumber(this.school_class));
        }

}
}
