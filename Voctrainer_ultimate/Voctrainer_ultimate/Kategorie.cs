﻿//Class name: Kategorie
//Author: Moedlagl
//last change: 19.05.2016
//Description: 
//-----------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voctrainer_ultimate
{
    class Kategorie
    {
        private static Kategorie kat=null;

        public static Kategorie GetInstance()
        {
            if (kat == null)
                kat = new Kategorie();

            return kat;
        }

        
    }
}
