﻿//Class name: Logic
//Author: Moedlagl
//last change: 19.05.2016
//Description: This Class with it's methods is written to read the words from the database,
//compare the "Benutzereingaben" and calculate the mark
//-----------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.OleDb;

namespace Voctrainer_ultimate
{
    class Logic//Logic class calls all methods that are not directly involved in the graphic user interface
    {
        public List<Vokabel> ReadFromFile(string strFileOrPath)
        {
            List<Vokabel> list = new List<Vokabel>();
            OleDbCommand cmd = new OleDbCommand();
            OleDbConnection conn = new OleDbConnection();
            conn.Open();
            cmd.Connection = conn;
            cmd.CommandText = "select en, de from Vokabel where id = "/* hier noch Kategorie*/;




            //----------------------------
            return list;
            
        }
        /*public List<Vokabel> ReadFromFile(string strFileOrPath)
        {
            List<Vokabel> list = new List<Vokabel>();
            StreamReader sr = new StreamReader(strFileOrPath);
            string Row;
            int Index;

            try
            {
                while (sr.Peek() != -1)
                {
                    Row = sr.ReadLine();
                    Index = Row.IndexOf(';');
                    list.Add(new Vokabel(Row.Substring(0, Index), Row.Substring(Index + 1)));
                }

                sr.Close();

            }

            // Datei nicht gefunden

            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Verzeichnis existiert nicht

            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Pfadangabe war 'null'

            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Pfadangabe war leer ("")

            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // allgemeine Exception

            catch (Exception ex)
            {
                
                Console.WriteLine(ex.Message);
                System.Threading.Thread.Sleep(1500);
            }
            finally
            {
                
            }
            return list;
        }
        */



        public bool CheckEntries(string a, string b)
        {
            if (a == b)
                return true;
            else
                return false;
        }

        public void SortEntries(ref List<Vokabel> listEntries)
        {
            Random rnd = new Random();

            foreach (Vokabel v in listEntries)
            {
                v.Rand = rnd.Next(1, listEntries.Count);
            }

            listEntries.Sort(delegate (Vokabel a, Vokabel b)
            {
                return a.Rand.CompareTo(b.Rand);
            });
        }

        public void CalculateMark(out double mark, ref string text, Option o, ref  double rightAnswer, int count)
        {
            if (o.MaxEntries > count)
                mark = (rightAnswer / count) * 100;
            else
                mark = (rightAnswer / o.MaxEntries) * 100;

            for (int j = 0; j < o.GradePercent.Length; j++)
                if (mark > o.GradePercent[j])
                {
                    text = o.GradeText[j];
                    break;
                }

            if (text == "error")
                text = o.GradeText[4];

            rightAnswer = 0;
        }
    }
}
