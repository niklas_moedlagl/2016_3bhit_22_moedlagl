﻿//Class name: Form1
//Author: Moedlagl
//last change:
//-----------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Voctrainer_ultimate
{
    public partial class Form1 : Form
    {
        private List<Vokabel> list;
        private Panel Entries;
        Option o = Option.getInstance();
        Logic logic;
        double rightAnswer = 0;

        public Form1()
        {
            InitializeComponent();
            logic = new Logic();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            CheckEntries();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            DeletePanel();
            logic.SortEntries(ref list);
            CreateEntries(list);
        }

        private void CreateEntries(List<Vokabel> listEntries)
        {
            Entries = new Panel();
            Entries.Width = 450;
            Entries.Height = 10;
            Entries.Top = 20;

            this.Height = 100;
            this.Controls.Add(Entries);

            int Topspace = 20;
            int i = 0;

            foreach (Vokabel voc in listEntries)
            {
                if (i < o.MaxEntries)
                {
                    Label lblWord = new Label();
                    if (o.TranslationMode == true)
                        lblWord.Text = voc.German;
                    else if (o.TranslationMode == false)
                        lblWord.Text = voc.English;
                    lblWord.Width = 120;
                    lblWord.Height = 40;
                    lblWord.Left = 20;
                    lblWord.Top = Topspace;
                    Topspace += 10 + lblWord.Height;

                    TextBox txtWord = new TextBox();
                    txtWord.Tag = voc;


                    txtWord.Left = lblWord.Left + lblWord.Width + 20;
                    txtWord.Top = lblWord.Top;
                    txtWord.Width = lblWord.Width;
                    txtWord.Height = lblWord.Height;

                    this.Height += 10 + lblWord.Height;
                    Entries.Height += 10 + lblWord.Height;

                    Entries.Controls.Add(lblWord);
                    Entries.Controls.Add(txtWord);

                    btnCheck.Top = Topspace;
                    btnReload.Top = btnCheck.Top;
                }

                i++;
            }
        }

        private void CheckEntries()
        {
            foreach (Control c in this.Entries.Controls)
            {
                if (c is TextBox)
                {
                    Vokabel v = (Vokabel)c.Tag;
                    if (o.TranslationMode == true)
                    {
                        if (logic.CheckEntries(v.English.ToLower(), c.Text.ToLower()))
                        {
                            c.BackColor = Color.LightGreen;
                            rightAnswer++;
                        }
                        else
                            c.BackColor = Color.Tomato;
                    }
                    else
                    { 
                        if (logic.CheckEntries(v.German.ToLower(), c.Text.ToLower()))
                        {
                            c.BackColor = Color.LightGreen;
                            rightAnswer++;
                        }
                        else
                            c.BackColor = Color.Tomato;
                    }
                }
            }

            double mark;
            string text = "error";

            logic.CalculateMark(out mark, ref text, o, ref rightAnswer, list.Count);

            lblMarkPercent.Text = Convert.ToString(mark) + "%";
            lblMarkText.Text = text;
        }
        private void DeletePanel()
        {
            this.Controls.Remove(Entries);
        }

        private void msiopen_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Vokabel-Dateien |*.voc";
            open.Title = "Datei auswählen:";

            if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (list != null)
                {
                    DeletePanel();
                }
                list = logic.ReadFromFile(open.FileName);
                logic.SortEntries(ref list);
                CreateEntries(list);
            }

        }

        private void msiclose_Click(object sender, EventArgs e)
        {

            if (list == null)
                this.Close();

            if (MessageBox.Show("Wollen Sie das Programm wirklich beenden?", "Vokabeltrainer beenden?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                this.Close();

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (list == null)
                e.Cancel = false;

            else if (MessageBox.Show("Wollen Sie das Programm wirklich beenden?", "Vokabeltrainer beenden?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                e.Cancel = false;
            else
                e.Cancel = true;
        }

        private void msiproperties_Click(object sender, EventArgs e)
        {
            Option o = Option.getInstance();

            DialogResult result = o.ShowDialog();
            if (result == DialogResult.OK)
                MessageBox.Show("Saved!");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}