﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Voctrainer_ultimate
{
    public partial class Option : Form
    {
        private bool isDirty;
        double maxEntries;
        bool translationMode;
        int[] gradePercent = new int[4];
        string[] gradeText = new string[5];
        static Option opt = null;

        public double MaxEntries
        {
            set { maxEntries = value; }
            get { return maxEntries; }
        }

        public bool TranslationMode
        {
            set { translationMode = value; }
            get { return translationMode; }
        }

        public int[] GradePercent
        {
            set { gradePercent = value; }
            get { return gradePercent; }
        }

        public string[] GradeText
        {
            set { gradeText = value; }
            get { return gradeText; }
        }

        private Option()
        {
            ReadOptions();
            InitializeComponent();
            nudvoccount.Value = Convert.ToInt32(MaxEntries);
            if (translationMode == true)
                cblanguage.Text = "DE > EN";
            else if (translationMode == false)
                cblanguage.Text = "EN > DE";
            nudperc1.Value = gradePercent[0];
            nudperc2.Value = gradePercent[1];
            nudperc3.Value = gradePercent[2];
            nudperc4.Value = gradePercent[3];
            nudperc5.Value = 0;

            txtmark1.Text = gradeText[0];
            txtmark2.Text = gradeText[1];
            txtmark3.Text = gradeText[2];
            txtmark4.Text = gradeText[3];
            txtmark5.Text = gradeText[4];
        }

        public static Option getInstance()
        {
            if (opt == null)
                opt = new Option();

            return opt;
        }

        public void ReadOptions()
        {
            StreamReader sr = new StreamReader("config.conf");
            string[] splitted = sr.ReadToEnd().Split(';');

            if (splitted.Length == 12)
            {
                maxEntries = Convert.ToInt32(splitted[0]);
                translationMode = Convert.ToBoolean(splitted[1]);
                gradePercent[0] = Convert.ToInt32(splitted[2]);
                gradePercent[1] = Convert.ToInt32(splitted[4]);
                gradePercent[2] = Convert.ToInt32(splitted[6]);
                gradePercent[3] = Convert.ToInt32(splitted[8]);

                gradeText[0] = Convert.ToString(splitted[3]);
                gradeText[1] = Convert.ToString(splitted[5]);
                gradeText[2] = Convert.ToString(splitted[7]);
                gradeText[3] = Convert.ToString(splitted[9]);
                gradeText[4] = Convert.ToString(splitted[11]);
            }

            else
            {
                MessageBox.Show("Fehler beim Lesen der \"config.txt\"!\nEs werden vordefinierte Einstellungen verwendet...");
                maxEntries = 10;
                translationMode = true;
                gradePercent[0] = 85;
                gradePercent[1] = 75;
                gradePercent[2] = 65;
                gradePercent[3] = 50;

                gradeText[0] = "Perfect!";
                gradeText[1] = "Good!";
                gradeText[2] = "Okay";
                gradeText[3] = "Learn more!";
                gradeText[4] = "You are so bad!";
            }

            sr.Close();
        }

        public void WriteOptions()
        {
            StreamWriter sw = new StreamWriter("config.conf");
            sw.Write(maxEntries.ToString() + ";" + translationMode.ToString() + ";" + gradePercent[0].ToString() + ";" + gradeText[0] + ";" + gradePercent[1].ToString() + ";" + gradeText[1] + ";" + gradePercent[2].ToString() + ";" + gradeText[2] + ";" + gradePercent[3].ToString() + ";" + gradeText[3] + ";0;" + gradeText[4]);

            sw.Flush();
            sw.Close();
        }

        private void cblanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            isDirty = true;

            if (cblanguage.Text == "EN > DE")
                TranslationMode = false;

            else if (cblanguage.Text == "DE > EN")
                TranslationMode = true;
        }

        private void nudvoccount_ValueChanged(object sender, EventArgs e)
        {
            MaxEntries = Convert.ToInt32(nudvoccount.Value);
            isDirty = true;
        }

        private void btnSaveQuit_Click(object sender, EventArgs e)
        {
            if (isDirty == true)
            {
                WriteOptions();
                isDirty = false;
                MessageBox.Show("Saved!");
            }
            this.Close();
        }

        private void Options_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isDirty == true)
            {
                WriteOptions();
                isDirty = false;
                MessageBox.Show("Saved!");
            }
        }

        private void nudperc1_ValueChanged(object sender, EventArgs e)
        {
            gradePercent[0] = Convert.ToInt32(nudperc1.Value);
            isDirty = true;
        }

        private void nudperc2_ValueChanged(object sender, EventArgs e)
        {
            gradePercent[1] = Convert.ToInt32(nudperc2.Value);
            isDirty = true;
        }

        private void nudperc3_ValueChanged(object sender, EventArgs e)
        {
            gradePercent[2] = Convert.ToInt32(nudperc3.Value);
            isDirty = true;
        }

        private void nudperc4_ValueChanged(object sender, EventArgs e)
        {
            gradePercent[3] = Convert.ToInt32(nudperc4.Value);
            isDirty = true;
        }

        private void txtmark1_TextChanged(object sender, EventArgs e)
        {
            gradeText[0] = Convert.ToString(txtmark1.Text);
            isDirty = true;
        }

        private void txtmark2_TextChanged(object sender, EventArgs e)
        {
            gradeText[1] = Convert.ToString(txtmark2.Text);
            isDirty = true;
        }

        private void txtmark3_TextChanged(object sender, EventArgs e)
        {
            gradeText[2] = Convert.ToString(txtmark3.Text);
            isDirty = true;
        }

        private void txtmark4_TextChanged(object sender, EventArgs e)
        {
            gradeText[3] = Convert.ToString(txtmark4.Text);
            isDirty = true;
        }

        private void txtmark5_TextChanged(object sender, EventArgs e)
        {
            gradeText[4] = Convert.ToString(txtmark5.Text);
            isDirty = true;
        }
    }
}
