﻿namespace Voctrainer_ultimate
{
    partial class Option
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudvoccount = new System.Windows.Forms.NumericUpDown();
            this.lblvoccount = new System.Windows.Forms.Label();
            this.lbllanguage = new System.Windows.Forms.Label();
            this.cblanguage = new System.Windows.Forms.ComboBox();
            this.btnSaveQuit = new System.Windows.Forms.Button();
            this.nudperc1 = new System.Windows.Forms.NumericUpDown();
            this.nudperc2 = new System.Windows.Forms.NumericUpDown();
            this.nudperc3 = new System.Windows.Forms.NumericUpDown();
            this.nudperc4 = new System.Windows.Forms.NumericUpDown();
            this.nudperc5 = new System.Windows.Forms.NumericUpDown();
            this.txtmark1 = new System.Windows.Forms.TextBox();
            this.txtmark2 = new System.Windows.Forms.TextBox();
            this.txtmark3 = new System.Windows.Forms.TextBox();
            this.txtmark4 = new System.Windows.Forms.TextBox();
            this.txtmark5 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudvoccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc5)).BeginInit();
            this.SuspendLayout();
            // 
            // nudvoccount
            // 
            this.nudvoccount.Location = new System.Drawing.Point(235, 17);
            this.nudvoccount.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudvoccount.Name = "nudvoccount";
            this.nudvoccount.Size = new System.Drawing.Size(43, 20);
            this.nudvoccount.TabIndex = 0;
            this.nudvoccount.ValueChanged += new System.EventHandler(this.nudvoccount_ValueChanged);
            // 
            // lblvoccount
            // 
            this.lblvoccount.AutoSize = true;
            this.lblvoccount.Location = new System.Drawing.Point(13, 19);
            this.lblvoccount.Name = "lblvoccount";
            this.lblvoccount.Size = new System.Drawing.Size(99, 13);
            this.lblvoccount.TabIndex = 1;
            this.lblvoccount.Text = "Anzahl der Vokabel";
            // 
            // lbllanguage
            // 
            this.lbllanguage.AutoSize = true;
            this.lbllanguage.Location = new System.Drawing.Point(13, 55);
            this.lbllanguage.Name = "lbllanguage";
            this.lbllanguage.Size = new System.Drawing.Size(172, 13);
            this.lbllanguage.TabIndex = 2;
            this.lbllanguage.Text = "Sprache der zu fragenden Vokabel";
            // 
            // cblanguage
            // 
            this.cblanguage.FormattingEnabled = true;
            this.cblanguage.Items.AddRange(new object[] {
            "DE > EN",
            "EN > DE"});
            this.cblanguage.Location = new System.Drawing.Point(191, 52);
            this.cblanguage.Name = "cblanguage";
            this.cblanguage.Size = new System.Drawing.Size(121, 21);
            this.cblanguage.TabIndex = 3;
            this.cblanguage.SelectedIndexChanged += new System.EventHandler(this.cblanguage_SelectedIndexChanged);
            // 
            // btnSaveQuit
            // 
            this.btnSaveQuit.Location = new System.Drawing.Point(256, 301);
            this.btnSaveQuit.Name = "btnSaveQuit";
            this.btnSaveQuit.Size = new System.Drawing.Size(75, 23);
            this.btnSaveQuit.TabIndex = 4;
            this.btnSaveQuit.Text = "Ok";
            this.btnSaveQuit.UseVisualStyleBackColor = true;
            this.btnSaveQuit.Click += new System.EventHandler(this.btnSaveQuit_Click);
            // 
            // nudperc1
            // 
            this.nudperc1.Location = new System.Drawing.Point(12, 104);
            this.nudperc1.Name = "nudperc1";
            this.nudperc1.Size = new System.Drawing.Size(120, 20);
            this.nudperc1.TabIndex = 6;
            this.nudperc1.ValueChanged += new System.EventHandler(this.nudperc1_ValueChanged);
            // 
            // nudperc2
            // 
            this.nudperc2.Location = new System.Drawing.Point(13, 131);
            this.nudperc2.Name = "nudperc2";
            this.nudperc2.Size = new System.Drawing.Size(120, 20);
            this.nudperc2.TabIndex = 7;
            this.nudperc2.ValueChanged += new System.EventHandler(this.nudperc2_ValueChanged);
            // 
            // nudperc3
            // 
            this.nudperc3.Location = new System.Drawing.Point(13, 158);
            this.nudperc3.Name = "nudperc3";
            this.nudperc3.Size = new System.Drawing.Size(120, 20);
            this.nudperc3.TabIndex = 8;
            this.nudperc3.ValueChanged += new System.EventHandler(this.nudperc3_ValueChanged);
            // 
            // nudperc4
            // 
            this.nudperc4.Location = new System.Drawing.Point(13, 185);
            this.nudperc4.Name = "nudperc4";
            this.nudperc4.Size = new System.Drawing.Size(120, 20);
            this.nudperc4.TabIndex = 9;
            this.nudperc4.ValueChanged += new System.EventHandler(this.nudperc4_ValueChanged);
            // 
            // nudperc5
            // 
            this.nudperc5.Location = new System.Drawing.Point(13, 212);
            this.nudperc5.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.nudperc5.Name = "nudperc5";
            this.nudperc5.Size = new System.Drawing.Size(120, 20);
            this.nudperc5.TabIndex = 10;
            // 
            // txtmark1
            // 
            this.txtmark1.Location = new System.Drawing.Point(191, 103);
            this.txtmark1.Name = "txtmark1";
            this.txtmark1.Size = new System.Drawing.Size(100, 20);
            this.txtmark1.TabIndex = 11;
            this.txtmark1.TextChanged += new System.EventHandler(this.txtmark1_TextChanged);
            // 
            // txtmark2
            // 
            this.txtmark2.Location = new System.Drawing.Point(191, 130);
            this.txtmark2.Name = "txtmark2";
            this.txtmark2.Size = new System.Drawing.Size(100, 20);
            this.txtmark2.TabIndex = 12;
            this.txtmark2.TextChanged += new System.EventHandler(this.txtmark2_TextChanged);
            // 
            // txtmark3
            // 
            this.txtmark3.Location = new System.Drawing.Point(191, 157);
            this.txtmark3.Name = "txtmark3";
            this.txtmark3.Size = new System.Drawing.Size(100, 20);
            this.txtmark3.TabIndex = 13;
            this.txtmark3.TextChanged += new System.EventHandler(this.txtmark3_TextChanged);
            // 
            // txtmark4
            // 
            this.txtmark4.Location = new System.Drawing.Point(191, 184);
            this.txtmark4.Name = "txtmark4";
            this.txtmark4.Size = new System.Drawing.Size(100, 20);
            this.txtmark4.TabIndex = 14;
            this.txtmark4.TextChanged += new System.EventHandler(this.txtmark4_TextChanged);
            // 
            // txtmark5
            // 
            this.txtmark5.Location = new System.Drawing.Point(191, 211);
            this.txtmark5.Name = "txtmark5";
            this.txtmark5.Size = new System.Drawing.Size(100, 20);
            this.txtmark5.TabIndex = 15;
            this.txtmark5.TextChanged += new System.EventHandler(this.txtmark5_TextChanged);
            // 
            // Option
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 336);
            this.Controls.Add(this.txtmark5);
            this.Controls.Add(this.txtmark4);
            this.Controls.Add(this.txtmark3);
            this.Controls.Add(this.txtmark2);
            this.Controls.Add(this.txtmark1);
            this.Controls.Add(this.nudperc5);
            this.Controls.Add(this.nudperc4);
            this.Controls.Add(this.nudperc3);
            this.Controls.Add(this.nudperc2);
            this.Controls.Add(this.nudperc1);
            this.Controls.Add(this.btnSaveQuit);
            this.Controls.Add(this.cblanguage);
            this.Controls.Add(this.lbllanguage);
            this.Controls.Add(this.lblvoccount);
            this.Controls.Add(this.nudvoccount);
            this.Name = "Option";
            this.Text = "Optionen";
            ((System.ComponentModel.ISupportInitialize)(this.nudvoccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudperc5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown nudvoccount;
        private System.Windows.Forms.Label lblvoccount;
        private System.Windows.Forms.Label lbllanguage;
        private System.Windows.Forms.ComboBox cblanguage;
        private System.Windows.Forms.Button btnSaveQuit;
        private System.Windows.Forms.NumericUpDown nudperc1;
        private System.Windows.Forms.NumericUpDown nudperc2;
        private System.Windows.Forms.NumericUpDown nudperc3;
        private System.Windows.Forms.NumericUpDown nudperc4;
        private System.Windows.Forms.NumericUpDown nudperc5;
        private System.Windows.Forms.TextBox txtmark1;
        private System.Windows.Forms.TextBox txtmark2;
        private System.Windows.Forms.TextBox txtmark3;
        private System.Windows.Forms.TextBox txtmark4;
        private System.Windows.Forms.TextBox txtmark5;
    }
}