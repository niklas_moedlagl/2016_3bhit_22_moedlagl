﻿//Class name: Database
//Author: Moedlagl
//last change: 19.05.2016
//Description: 
//-----------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace Voctrainer_ultimate
{
    class Database
    {
        private Database dat = null;
        OleDbConnection dbcn;

        private Database()
        {
            dbcn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Vokabeltrainer.accdb;"/*, this.valid*/);   
        }

        public static Database GetInstance(Database dat)
        {
            if(dat==null)
                dat = new Database();

            return dat;
        }

        public void GetItems(OleDbConnection dat, List<Vokabel> list)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = dat;
            cmd.CommandText = "SELECT * FROM Vokabel WHERE Vokabel.Kategorie=Kategorie.ID;";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
                list.Add(new Vokabel(/*reader.GetInt32(0)*/ reader.GetString(1), reader.GetString(2)));

        }

    }
}