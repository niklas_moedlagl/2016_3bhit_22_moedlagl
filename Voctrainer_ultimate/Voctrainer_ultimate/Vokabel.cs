﻿//Class name: Vokabel
//Author: Moedlagl
//last change: 19.05.2016
//Description: This Class is the blueprint for the vocabulary words which are in the database
//
//-----------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Voctrainer_ultimate
{
    class Vokabel
    {
        private string Germanword;

        public string German
        {
            get { return Germanword; }
            set { Germanword = value; }
        }
        private string Englishword;

        public string English
        {
            get { return Englishword; }
            set { Englishword = value; }
        }
        private double dRand;

        public double Rand
        {
            get { return dRand; }
            set { dRand = value; }
        }

        private int Kategorie;

        public int Kategorienumb
        {
            get { return Kategorie; }
            set { Kategorie = value; }
        }


        public Vokabel(string Germanword, string Englishword)
        {
            German = Germanword;
            English = Englishword;
            Rand = 0;
        }
    }
}